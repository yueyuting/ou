﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OU.OnlineUpdate
{
    public static class Tools
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="byteArray"></param>
        /// <param name="byteposition">字节数</param>
        /// <param name="bitposition">位数</param>
        /// <param name="value"></param>
        public static void SetBitInByteArray(this byte[] byteArray, byte byteposition, byte bitposition, bool value)
        {
            if (bitposition > 7 || bitposition < 0)
                throw new Exception("字节索引超出范围");
            if (value)
                byteArray[byteposition] |= (byte)(1 << bitposition);
            else
                byteArray[byteposition] &= (byte)(~(1 << bitposition));
        }
    }
}
