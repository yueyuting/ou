﻿namespace OU.OnlineUpdate
{
    public static class Data
    {
        //正常通讯协议
        public static byte[] SendDataDefalut =
        [
        0xAA,0x55,0x1A,0x40,0x00,0x00,0x00,0x33,0x02,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x14,0x18,0x00,0x07,0x00,0x18,0x09,0x38,0x3A,0x31,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xBD,0xBE,0x00,0x00,0x00,0x02,0x00,0x00,0x02,0xFC
        ];

        //下发程序协议
        public static byte[] SendDataInclueBit = new byte[128];
        public static void SetSendHead(ushort dataid, ushort alllen)
        {

            SendDataInclueBit[0] = 0xAA;
            SendDataInclueBit[1] = 0x55;
            if (SendDataInclueBit[2] < 255)
                SendDataInclueBit[2]++;
            else SendDataInclueBit[2] = 0;
            SendDataInclueBit[3] = 0x80;
            //反馈帧计数
            SendDataInclueBit[5] = 0;

            //STOF包状态字-系统模式和状态字
            SendDataInclueBit[6] = SendDataDefalut[60];
            SendDataInclueBit[7] = SendDataDefalut[61];

            //当前包目标码字节数
            //SendDataInclueBit[14] = 0x40;
            SendDataInclueBit.SetBitInByteArray(15, 4, false);//清除OFP加载标志请求
            SendDataInclueBit.SetBitInByteArray(15, 5, false);//执行加载请求
            SendDataInclueBit.SetBitInByteArray(15, 6, false);//OFP加载模式进入请求

            // 将数字转换为字节，并使用小端格式存储到数组中  
            byte[] numberBytes = BitConverter.GetBytes(dataid);
            if (BitConverter.IsLittleEndian)
            {
                SendDataInclueBit[16] = numberBytes[0];
                SendDataInclueBit[17] = numberBytes[1];
            }
            else
            {
                // 如果系统不是小端，需要交换字节顺序  
                SendDataInclueBit[16] = numberBytes[1];
                SendDataInclueBit[17] = numberBytes[0];
            }
            byte[] numberBytes1 = BitConverter.GetBytes(alllen);
            if (BitConverter.IsLittleEndian)
            {
                SendDataInclueBit[18] = numberBytes1[0];
                SendDataInclueBit[19] = numberBytes1[1];
            }
            else
            {
                // 如果系统不是小端，需要交换字节顺序  
                SendDataInclueBit[18] = numberBytes1[1];
                SendDataInclueBit[19] = numberBytes1[0];
            }


        }

        public static Model ModelNow = Model.正常运行;
        public enum Model
        {
            正常运行,
            在线加载模式,
            加载进行中,
            加载完成
        }
    }
    public class SplitData
    {
        public ushort SplitLength { get; set; }
        public byte[] Data { get; set; }

        public SplitData(ushort splitLength, byte[] data)
        {
            SplitLength = splitLength;
            Data = data;
        }
    }
}
