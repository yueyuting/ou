﻿using Anotar.NLog;
using MethodTimer;
using NLog;
using SwallowExceptions.Fody;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FossilEnergyTesting.Fody
{
    public static class MethodTimeLogger
    {
        public static void Log(MethodBase methodBase, TimeSpan elapsed, string message)
        {
            LogTo.Debug($"{methodBase.Name}, {elapsed}, {message}");
            // Console.WriteLine($"{methodBase.Name}, {elapsed}, {message}");
        }
    }

    public class MyClass
    {
        [Time("构造函数")]
        public MyClass() { }

        [Time("日志")]
        [LogToErrorOnException, SwallowExceptions]
        public void MyMethod()
        {
            LogTo.Debug("测试");
            throw new NotImplementedException();

            //Some code u are curious how long it takes
            Console.WriteLine("Hello");
        }
    }
}
