﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OU.SerialPort.Com
{
    public interface IComLib
    {
        public bool SendMessge(byte[] bytes);
        public Task<bool> SendMessgeAsync(byte[] bytes);
        public string OpenCloseCom(string portName);

        public event Action<byte[]> MessageReceive;
    }
}
